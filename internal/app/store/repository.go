package store

import "sandbox/http-rest-api/internal/app/models"

// UserRepository ...
type UserRepository interface {
	Create(*models.User) error
	Find(int) (*models.User, error)
	FindByEmail(string) (*models.User, error)
}
